package com.example.fanuelwahjudi.fanuelhomework;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.fanuelwahjudi.fanuelhomework.Interface.SelectedNumberListener;
import com.example.fanuelwahjudi.fanuelhomework.adapter.ListAnimalsAdapter;
import com.example.fanuelwahjudi.fanuelhomework.dataholder.GroupContainer;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fanuel Wahjudi on 8/7/2018.
 */

public class CreateGroupActivity extends AppCompatActivity {

    //view
    private ImageView groupPhoto_imgBtn;
    private EditText groupName_editText;
    private Button create_button;
    private ListView animals_listView;

    private int mSelectedNumber;
    private Integer[] mSelecetedItems;
    private static int PICK_IMAGE = 99;
    private String mImageUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_group);
        groupPhoto_imgBtn = (ImageView) findViewById(R.id.creategrup_imageButton);
        groupName_editText = (EditText) findViewById(R.id.creategrup_editText);
        create_button = (Button) findViewById(R.id.creategrup_button);
        animals_listView = (ListView) findViewById(R.id.creategrup_listview);

        mSelectedNumber = 0;
        animals_listView.setAdapter(new ListAnimalsAdapter(this, new SelectedNumberListener() {
            @Override
            public void onChangeSelectedNumber(int selectedNumber, Integer[] selectedItems) {
                mSelectedNumber = selectedNumber;
                mSelecetedItems = selectedItems;
                if(selectedNumber >= 2 && groupName_editText.getText().toString().length() > 0)
                    create_button.setEnabled(true);
                else
                    create_button.setEnabled(false);
            }
        }));

        groupName_editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().length() > 0 && mSelectedNumber >=2){
                    create_button.setEnabled(true);
                }else{
                    create_button.setEnabled(false);
                }
            }
        });

        create_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mSelectedNumber > 0){
                    List<Integer> selectedDataList = new ArrayList<Integer>();

                    for(int i = 0 ;i< mSelecetedItems.length;i++){
                        if(mSelecetedItems[i]==1){
                            selectedDataList.add(i);
                        }
                    }
                    Integer[] selectedDataRaw = new Integer[selectedDataList.size()];
                    selectedDataList.toArray(selectedDataRaw);
                    GroupContainer newData = new GroupContainer(groupName_editText.getText().toString(),mImageUrl,selectedDataRaw);
                    StaticVariable.listGroupData.add(newData);
                }
                Intent mIntent = new Intent(view.getContext(),MainActivity.class);
                startActivity(mIntent);
                finish();
            }
        });

        groupPhoto_imgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data!= null) {


            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            mImageUrl = cursor.getString(columnIndex);
            cursor.close();
            Bitmap mBitmap = null;
            try {
                mBitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));
                groupPhoto_imgBtn.setImageBitmap(mBitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onBackPressed() {
        Intent mIntent = new Intent(this,MainActivity.class);
        startActivity(mIntent);
        finish();
        super.onBackPressed();
    }
}
