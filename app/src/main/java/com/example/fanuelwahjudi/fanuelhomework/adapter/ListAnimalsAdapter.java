package com.example.fanuelwahjudi.fanuelhomework.adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fanuelwahjudi.fanuelhomework.Interface.SelectedNumberListener;
import com.example.fanuelwahjudi.fanuelhomework.R;
import com.example.fanuelwahjudi.fanuelhomework.StaticVariable;

/**
 * Created by Fanuel Wahjudi on 8/7/2018.
 */



public class ListAnimalsAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private Integer[] flag;
    private int selectedNumber;
    SelectedNumberListener mSelectedNumberListener;

    public ListAnimalsAdapter(Context mContext, SelectedNumberListener mSelectedNumberListener){
        this.mContext = mContext;
        this.mSelectedNumberListener = mSelectedNumberListener;
        flag = new Integer[StaticVariable.allAnimals.length];
        selectedNumber = 0;
        for (int i = 0; i < StaticVariable.allAnimals.length; i++) {
            flag[i] = 0;
        }

    }

    @Override
    public int getCount() {
        return StaticVariable.allAnimals.length;
    }

    @Override
    public Object getItem(int i) {
        return StaticVariable.allAnimals[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(view ==null){
            view = new View(mContext);
            view = mInflater.inflate(R.layout.create_group_item,null);
        }
        final ImageView mImage = (ImageView) view.findViewById(R.id.createGroupItem_imageview);
        final TextView mText = (TextView) view.findViewById(R.id.createGroupItem_textview);
        final ImageButton mImgBtn = (ImageButton) view.findViewById(R.id.createGroupItem_btn);

        mImage.setImageResource(StaticVariable.allPics[i]);
        mText.setText(StaticVariable.allAnimals[i]);
        final int index = i;
        if(flag[index] == 0) {
            mImgBtn.setImageResource(android.R.drawable.ic_input_add);
        }else {
            mImgBtn.setImageResource(android.R.drawable.ic_input_delete);
        }
        mImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mImgBtn.getDrawable().getConstantState() == mContext.getResources().getDrawable(android.R.drawable.ic_input_add).getConstantState()){
                    mImgBtn.setImageResource(android.R.drawable.ic_input_delete);
                    flag[index] = 1;
                    selectedNumber++;
                }else{
                    mImgBtn.setImageResource(android.R.drawable.ic_input_add);
                    flag[index] = 0;
                    selectedNumber--;
                }
                mSelectedNumberListener.onChangeSelectedNumber(selectedNumber,flag);
            }
        });

        //mImage.setImageDrawable();
        return view;
    }
}

