package com.example.fanuelwahjudi.fanuelhomework;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.fanuelwahjudi.fanuelhomework.adapter.ListGroupAdapter;

public class MainActivity extends AppCompatActivity {

    // view
    private FloatingActionButton addGroup_fab;
    private GridView listGroup_gridView;

    //
    public static String SELECTED_GROUP = "SELECTED_GROUP";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(StaticVariable.listGroupData.size()==0) StaticVariable.initiateGroup();
        listGroup_gridView = (GridView) findViewById(R.id.listGroup_gridview);
        listGroup_gridView.setAdapter(new ListGroupAdapter(this));
        listGroup_gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent mIntent = new Intent(view.getContext(),GroupDetailActivity.class);
                mIntent.putExtra(SELECTED_GROUP,i);
                startActivityForResult(mIntent,0);
            }
        });

        addGroup_fab = (FloatingActionButton) findViewById(R.id.addGroup_fab);
        addGroup_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(view.getContext(),CreateGroupActivity.class);
                startActivity(mIntent);
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
