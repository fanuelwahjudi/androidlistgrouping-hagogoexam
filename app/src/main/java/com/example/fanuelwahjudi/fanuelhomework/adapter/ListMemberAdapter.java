package com.example.fanuelwahjudi.fanuelhomework.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fanuelwahjudi.fanuelhomework.R;
import com.example.fanuelwahjudi.fanuelhomework.StaticVariable;

/**
 * Created by Fanuel Wahjudi on 8/7/2018.
 */



public class ListMemberAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private Integer[] member;

    public ListMemberAdapter(Context mContext, Integer[] member){
        this.mContext = mContext;
        this.member = member;
    }

    @Override
    public int getCount() {
        return member.length;
    }

    @Override
    public Object getItem(int i) {
        return StaticVariable.allAnimals[member[i]];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(view ==null){
            view = new View(mContext);
            view = mInflater.inflate(R.layout.detail_group_item,null);
        }
        ImageView mImage = (ImageView) view.findViewById(R.id.groupDetailItem_imageview);
        TextView mText = (TextView) view.findViewById(R.id.groupDetailItem_textview);

        mImage.setImageResource(StaticVariable.allPics[member[i]]);
        mText.setText(StaticVariable.allAnimals[member[i]]);

        //mImage.setImageDrawable();
        return view;
    }
}
