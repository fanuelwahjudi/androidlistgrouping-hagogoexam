package com.example.fanuelwahjudi.fanuelhomework.dataholder;

/**
 * Created by Fanuel Wahjudi on 8/7/2018.
 */

public class GroupContainer {
    private String name;
    private String imageUrl;
    private Integer[] animals;

    public String getName() {
        return name;
    }

    public String getImagePath() {
        return imageUrl;
    }

    public Integer[] getAnimals() {
        return animals;
    }

    public GroupContainer(String name, String imageUrl, Integer[] animals) {
        this.name     = name;
        this.imageUrl = imageUrl;
        this.animals  = animals;
    }
}
