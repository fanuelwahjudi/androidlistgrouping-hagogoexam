package com.example.fanuelwahjudi.fanuelhomework.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.fanuelwahjudi.fanuelhomework.StaticVariable;
import com.example.fanuelwahjudi.fanuelhomework.R;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by Fanuel Wahjudi on 8/7/2018.
 */



public class ListGroupAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;

    public ListGroupAdapter(Context mContext){
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return StaticVariable.listGroupData.size();
    }

    @Override
    public Object getItem(int i) {
        return StaticVariable.listGroupData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(view ==null){
            view = new View(mContext);
            view = mInflater.inflate(R.layout.group_item,null);
        }
        ImageView mImage = (ImageView) view.findViewById(R.id.groupItem_imageview);
        TextView mText = (TextView) view.findViewById(R.id.groupItem_textview);
        TextView mInfo = (TextView) view.findViewById(R.id.groupItem_info_textview);
        mText.setText(StaticVariable.listGroupData.get(i).getName());
        mInfo.setText("(" + StaticVariable.listGroupData.get(i).getAnimals().length + " animals )");
        File imgFile = new File(StaticVariable.listGroupData.get(i).getImagePath());
        if(imgFile.exists()){
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(mContext.getContentResolver().openInputStream(Uri.fromFile(imgFile)));
                mImage.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }


        return view;
    }
}
