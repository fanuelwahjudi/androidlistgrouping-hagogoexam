package com.example.fanuelwahjudi.fanuelhomework;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.fanuelwahjudi.fanuelhomework.adapter.ListMemberAdapter;
import com.example.fanuelwahjudi.fanuelhomework.dataholder.GroupContainer;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by Fanuel Wahjudi on 8/7/2018.
 */

public class GroupDetailActivity extends AppCompatActivity {

    private int GROUP_ID;
    // view
    private ListView listAnimals_listView;
    private ImageView group_imageView;
    private TextView groupName_textView;
    private TextView groupInfo_textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Initiate View
        setContentView(R.layout.detail_group);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.detailgrup_toolbar);
        setSupportActionBar(toolbar);*/
        listAnimals_listView = (ListView) findViewById(R.id.detailgrup_exlistview);
        group_imageView = (ImageView) findViewById(R.id.detailgrup_imageview);
        groupName_textView = (TextView) findViewById(R.id.detailgrup_name_textview);
        groupInfo_textView = (TextView) findViewById(R.id.detailgrup_info_textview);

        // Load parameter data from main activity
        GROUP_ID = getIntent().getIntExtra(MainActivity.SELECTED_GROUP,0);
        GroupContainer mGroup = StaticVariable.listGroupData.get(GROUP_ID);

        listAnimals_listView.setAdapter(new ListMemberAdapter(this,mGroup.getAnimals()));
        groupName_textView.setText(mGroup.getName());
        groupInfo_textView.setText(mGroup.getAnimals().length + " animals are selected" );
        File imgFile = new File(mGroup.getImagePath());
        if(imgFile.exists()){
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(Uri.fromFile(imgFile)));
                group_imageView.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_delete) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
