package com.example.fanuelwahjudi.fanuelhomework.Interface;

/**
 * Created by Fanuel Wahjudi on 8/8/2018.
 */

public interface SelectedNumberListener {
    public void onChangeSelectedNumber(int selectedNumber, Integer[] selectedItems);
}
