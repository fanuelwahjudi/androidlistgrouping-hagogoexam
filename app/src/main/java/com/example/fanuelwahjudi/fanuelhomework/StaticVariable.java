package com.example.fanuelwahjudi.fanuelhomework;

import com.example.fanuelwahjudi.fanuelhomework.dataholder.GroupContainer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Fanuel Wahjudi on 8/7/2018.
 */

public class StaticVariable {

    public final static String[] allAnimals = {
            "Ant",          //0
            "Antelope",     //1
            "Armadillo",    //2
            "Baboon",       //3
            "Bat",          //4
            "Bear",         //5
            "Bee",          //6
            "Bird",         //7
            "Buffalo",      //8
            "Butterfly",    //9
            "Cat",          //10
            "Crab",         //11
            "Crocodile",    //12
            "Dog",          //13
            "Dragonfly",    //14
            "Duck",         //15
            "Elephant",     //16
            "Fox",          //17
            "Frog",         //18
            "Gorilla",      //19
            "Hyena",        //20
            "Jaguar",       //21
            "Kangaroo",     //22
            "Koala",        //23
            "Komodo",       //24
            "Lion",         //25
            "Mouse",        //26
            "Owl",          //27
            "Rabbit",       //28
            "Seal",         //29
            "Sheep",        //30
            "Snake",        //31
            "Whale",        //32
            "Zebra"};       //33

    public final static Integer[] allPics = {
            R.drawable.img0,
            R.drawable.img1,
            R.drawable.img2,
            R.drawable.img3,
            R.drawable.img4,
            R.drawable.img5,
            R.drawable.img6,
            R.drawable.img7,
            R.drawable.img8,
            R.drawable.img9,
            R.drawable.img10,
            R.drawable.img11,
            R.drawable.img12,
            R.drawable.img13,
            R.drawable.img14,
            R.drawable.img15,
            R.drawable.img16,
            R.drawable.img17,
            R.drawable.img18,
            R.drawable.img19,
            R.drawable.img20,
            R.drawable.img21,
            R.drawable.img22,
            R.drawable.img23,
            R.drawable.img24,
            R.drawable.img25,
            R.drawable.img26,
            R.drawable.img27,
            R.drawable.img28,
            R.drawable.img29,
            R.drawable.img30,
            R.drawable.img31,
            R.drawable.img32,
            R.drawable.img33};


    public static List<GroupContainer> listGroupData = new ArrayList<GroupContainer>();

    public static void initiateGroup(){
        Integer[] carnivore_int = {5,12,20,21,24,25,27,31};
        Integer[] insect_int    = {0,6,9,14};
        Integer[] skyruler_int  = {4,7,27};
        GroupContainer carnivore = new GroupContainer("Carnivore","",carnivore_int);
        GroupContainer insect    = new GroupContainer("Insect","",insect_int);
        GroupContainer skyruler  = new GroupContainer("Sky Ruler","",skyruler_int);
        listGroupData.add(carnivore);
        listGroupData.add(insect);
        listGroupData.add(skyruler);


    }
}
